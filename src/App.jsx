import React, { Component } from "react";
import "App.css";
import Camera from "components/Camera";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome</h1>
        </header>
        <Camera video={{ width: 200, height: 200 }} imageFormat="image/jpg" />
      </div>
    );
  }
}

export default App;
