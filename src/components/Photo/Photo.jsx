import React from "react";
import PropTypes from "prop-types";
import "components/Photo/Photo.css";

class Photo extends React.Component {
  
  handleRemovePicture = () => {
    this.props.removePictureCallback(this.props.photo.id);
  };
  
  render() {
    
    return (
      <div className="gallery">
        <span onClick={this.handleRemovePicture} className="close">
          &times;
        </span>
        <img alt="" src={this.props.photo.img.src} />
      </div>
    );
  }
}
/*const Photo = ({ photo,removePictureCallback }) => {
  return (
    <div className="gallery">
      <span onClick={() => removePictureCallback(photo.id)} className="close">&times;</span>
      <img alt="" src={photo.img.src} />
    </div>
  );
};
*/
Photo.PropTypes = {
  photo: PropTypes.shape({
    src: PropTypes.string
  })
};
export default Photo;
