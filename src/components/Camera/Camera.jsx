import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from "components/Button/";
import PhotoList from "components/PhotoList";

class Camera extends Component {
  state = {
    pictures: []
  };
  /*Video width and height parameters will be used for canvas
    to keep them the same size  */
  setCanvas = () => {
    const canvas = document.createElement("canvas");
    const { width, height } = this.props.video;
    canvas.width = width;
    canvas.height = height;

    return canvas;
  };

  loadVideo = () => {
    const { audio, video } = this.props;
    const stream = this.videoStream;

    navigator.mediaDevices
      .getUserMedia({ audio, video })
      .then(mediaStream => {
        stream.srcObject = mediaStream;
        stream.load();
        stream.play();
      })
      .catch(err => console.log(err));
  };

  drawImage = () => {
    const canvas = this.setCanvas();
    const video = this.videoStream;
    const { imageFormat } = this.props.imageFormat;
    const context = canvas.getContext("2d");
    let image = new Image(canvas.width, canvas.height);

    context.drawImage(video, 0, 0, canvas.width, canvas.height);
    image.src = canvas.toDataURL(imageFormat);
    /*add a timestamp which will be used as a key in PhotoList component*/
    let timestamp = Date.now();
    let picture = {
      img: image,
      id: timestamp
    };

    return picture;
  };

  handleTakePicture = () => {
    let picture = this.drawImage();

    this.setState(prevState => ({
      pictures: prevState.pictures.concat([picture])
    }));
  };

  handleRemovePicture = pictureId => {
    this.setState(prevState => ({
      pictures: prevState.pictures.filter(picture => {
        return pictureId !== picture.id;
      })
    }));
  };

  componentDidMount() {
    this.loadVideo();
  }

  render() {
    return (
      <div>
        <video ref={video => (this.videoStream = video)} />
        <Button clickCallback={this.handleTakePicture} text="Take Photo" />
        <div>
          <PhotoList
            removePictureCallback={this.handleRemovePicture}
            pictures={this.state.pictures}
          />
        </div>
      </div>
    );
  }
}

Camera.PropTypes = {
  audio: PropTypes.bool,
  video: PropTypes.shape({
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
  }),
  imageFormat: PropTypes.oneOf(["image/jpg", "image/png", "image/webp"])
};

Camera.defaultProps = {
  audio: false,
  video: { width: 300, height: 300 },
  imageFormat: "image/webp"
};

export default Camera;
