import React from "react";
import PropTypes from "prop-types";

const Button = ({ text, clickCallback }) => {
  return (
    <div>
      <button type="button" onClick={clickCallback}>
        {text}
      </button>
    </div>
  );
};

Button.PropTypes = {
  text: PropTypes.string,
  clickCallback: PropTypes.func
};

export default Button;
