import React from "react";
import PropTypes from "prop-types";
import Photo from "components/Photo";

const PhotoList = ({ pictures, removePictureCallback }) => {
  return pictures.map(picture => (
    <Photo
      removePictureCallback={removePictureCallback}
      key={picture.id.toString()}
      photo={picture}
    />
  ));
};

PhotoList.PropTypes = {
  pictures: PropTypes.shape({
    id: PropTypes.number,
    img: PropTypes.string
  })
};

export default PhotoList;
